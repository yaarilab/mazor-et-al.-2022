##################################################################################
################################# read LC files ##################################
##################################################################################
library(seqinr)
library(stringr)
library(alakazam)
library(shazam)
library(dplyr)
library(tigger)
library(parallel)
library(utils)
library(stringdist)
library(RColorBrewer)
library(gridExtra)
library(gplots)
library(parallel)

files.list <- c("1 (3)-Total Light - V2.aa",
             "2 (4)-Total Light - V2.aa",
             "3 (7)-Total Light - V3.aa",
             "4 (9)-Total Light - V2.aa")

cancer_data <- NULL #data frame file

for (f in files.list){
  temp <- read.fasta(file=f, as.string=TRUE)
  if(sum(grepl('|',names(temp),fixed=T)))
    names(temp) <- sapply(strsplit(names(temp),"|",fixed = T),"[",2)
  temp <- as.data.frame(toupper(unlist(temp)))
  temp$file_name <- f
  cancer_data <- rbind(cancer_data,temp)
}

names(cancer_data)[1] <- "sequence_input"
cancer_data$sequence_name <- paste0(cancer_data$file_name,"_",rownames(cancer_data))
rownames(cancer_data) <- c()

cancer_data$sequence_name <- str_replace_all(cancer_data$sequence_name," ","_")

save(cancer_data,file="cancer_data.rda")

############################################################################################################
############################ The constans parameters for the processing pipeline ###########################

### germline files with gaps!
VGERM_PATH_L <- "/germlines/LC/human_gl_IGLV_F+ORF+in-frame_P_w_gaps.fasta" 
VGERM_PATH_K <- "/germlines/LC/human_gl_IGKV_F+ORF+in-frame_P_w_gaps.fasta" 
DGERM_PATH <-  "/germlines/HC/IGHD.fasta" 
JGERM_PATH_L <- "/germlines/LC/human_gl_IGLJ_F+ORF+in-frame_P.fasta" 
JGERM_PATH_K <- "/germlines/LC/human_gl_IGKJ_F+ORF+in-frame_P.fasta" 


# make db parameters
makedb_path <- '/private/anaconda3/bin/MakeDb.py'
# the makedb repo currenty in use, is in the repository
makedb_repo <- 'MakeDb_IGL_repo.fasta'

# IgBlast directory
igblast_dir <- '/private/tools/igblast/ncbi-igblast-1.16.0/bin/' # 

########################################################################################################
########################################## run igblast #################################################

###### loading constant parameters and internal functions ###### 

source('internal_functions.R')

###### Initial germline for genotype and IgBLAST 1 with gaps! ###### 

VGERM_L <- read.fasta(VGERM_PATH_L,as.string = TRUE)
if(sum(grepl('|',names(VGERM_L),fixed=T)))
  names(VGERM_L) <- sapply(strsplit(names(VGERM_L),"|",fixed = T),"[",2)
VGERM_L <- toupper(unlist(VGERM_L))

VGERM_K <- read.fasta(VGERM_PATH_K,as.string = TRUE)
if(sum(grepl('|',names(VGERM_K),fixed=T)))
  names(VGERM_K) <- sapply(strsplit(names(VGERM_K),"|",fixed = T),"[",2)
VGERM_K <- toupper(unlist(VGERM_K))

JGERM_L <- read.fasta(JGERM_PATH_L,as.string = TRUE)
if(sum(grepl('|',names(JGERM_L),fixed=T)))
  names(JGERM_L) <- sapply(strsplit(names(JGERM_L),"|",fixed = T),"[",2)
JGERM_L <- toupper(unlist(JGERM_L))

JGERM_K <- read.fasta(JGERM_PATH_K,as.string = TRUE)
if(sum(grepl('|',names(JGERM_K),fixed=T)))
  names(JGERM_K) <- sapply(strsplit(names(JGERM_K),"|",fixed = T),"[",2)
JGERM_K <- toupper(unlist(JGERM_K))

VJ_GERM_K_L=c(VGERM_L,VGERM_K,JGERM_L,JGERM_K) #makedb need V and J germlines with gaps

write.fasta(sequences=as.list(VJ_GERM_K_L),
            names=names(VJ_GERM_K_L), "/lambda_LC/MakeDb_IGL_repo.fasta",open="w")

num_alignments_V=3 # number of V to get back from igblast aligner

###  convert the cancer data to a fasta file
save(cancer_data, file="/lambda_LC/cancer_data_l.rda")
write.fasta(sequences=as.list(gsub(cancer_data$sequence_input,pattern = '.',replacement = '',fixed = T)),
            names=cancer_data$sequence_name,"cancer_data_l.fasta",open="w")

############################### create germline files for igblast running ###############################################
## ./edit_imgt_file.pl /localdata/amit/cancer/process_germline/database2/IGHV_no_dup.fasta > /localdata/amit/cancer/process_germline/database2/human_gl_V.fasta
## ./makeblastdb -parse_seqids -dbtype nucl -in /localdata/amit/cancer/process_germline/database2/human_gl_V.fasta -out /localdata/amit/cancer/process_germline/ig_db/human_V

write.fasta(sequences=as.list(gsub(c(VGERM_L,VGERM_K),pattern = '.',replacement = '',fixed = T)),
            names=names(c(VGERM_L,VGERM_K)),"/germlines/LC/vgerm_l.fasta",open="w")
system(paste0(igblast_dir,'/makeblastdb -parse_seqids -dbtype nucl -in /germlines/LC/vgerm_l.fasta -out /germlines/LC/human_gl_V'))

### fictive IGHD sequences (from HC)
write.fasta(sequences=as.list(gsub(DGERM_H,pattern = '.',replacement = '',fixed = T)),
            names=names(DGERM_H),"/germlines//HC/IGHD.fasta",open="w")
system(paste0(igblast_dir,'/makeblastdb -parse_seqids -dbtype nucl -in /germlines//HC/IGHD.fasta -out /germlines/LC/human_gl_D'))

write.fasta(sequences=as.list(gsub(c(JGERM_L,JGERM_K),pattern = '.',replacement = '',fixed = T)),
            names=names(c(JGERM_L,JGERM_K)),"/germlines/LC/jgerm_l.fasta",open="w")
system(paste0(igblast_dir,'/makeblastdb -parse_seqids -dbtype nucl -in /germlines/LC/jgerm_l.fasta -out /germlines/LC/human_gl_J'))

#######################################

system(paste0("cd ",igblast_dir,"; ./igblastn ",
              " -num_alignments_V ",num_alignments_V,
              " -germline_db_V /germlines/LC/human_gl_V",
              " -germline_db_D /germlines/LC/human_gl_D",
              " -germline_db_J /germlines/LC/human_gl_J",
              " -organism human",
              " -domain_system imgt -query cancer_data_l.fasta",
              " -auxiliary_data /igblast/ncbi-igblast-1.16.0/optional_file/human_gl.aux",
              " -outfmt '7 std qseq sseq btop' -num_threads 44 -out /LC_data/igblast_1st.out")) 

# Run MakeDB to correct the numbers and alignments
system(paste0("cd ","/LC_data; ",
              paste0(makedb_path, ' igblast -i igblast_1st.out',
                     ' -s cancer_data_l.fasta',  ## s is the original fasta file we insert to IgBlast
                     ' -r /germlines/LC/MakeDb_IGL_repo.fasta --outdir ',  ## r is the IMGT fasta files with gaps
                     '/LC_data --outname cancer_1st_realigned --failed'))) 

# Read the files

DATA_r=read.delim("/LC_data/cancer_1st_realigned_db-pass.tsv", sep = "\t") #realigned sequences
DATA_f=read.delim("/LC_data/cancer_1st_realigned_db-fail.tsv", sep = "\t") #failed sequences

DATA=rbind(DATA_r%>%dplyr::mutate(status="pass"),DATA_f%>%mutate(status="fail"))

DATA$file_name=sapply(DATA$sequence_id, function(x) {str_split(string=x, pattern=".aa")[[1]][1]} )

## number of sequences that fail and pass per patient
DATA %>% group_by(file_name, status) %>% summarise (n=n())

## filter on sequences without v_call / j_call / junction
DATA <- DATA %>% filter(v_call!="" & j_call!="" & junction_length!=0)
DATA %>% group_by(file_name) %>% summarise (n=n())

DATA$v_call_gene=getGene(DATA$v_call, first = FALSE)
DATA$j_call_gene=getGene(DATA$j_call, first = FALSE)

DATA <- DATA %>% mutate(patient=substr(sequence_id,1,1)) %>% 
  arrange(patient, v_call_gene, j_call_gene, junction_length)

## remove OR genes
DATA <- DATA %>% filter(!grepl("OR",DATA$v_call_gene)) 

### for multiple assignment of V genes and J genes choose the gene which have another sequence with the same JL

for (i in 1:nrow(DATA)){
  if (grepl(",",DATA$j_call_gene[i])){
    temp <- DATA %>% filter(v_call_gene==v_call_gene[i], junction_length==junction_length[i], j_call_gene%in%str_split(DATA$j_call_gene[i],",")[[1]])
    if (nrow(temp)>=1){
      DATA$j_call_gene[i] <- temp$j_call_gene[1]
      print(i)
    }
  }
  if (grepl(",",DATA$v_call_gene[i])){
    temp <- DATA %>% filter(j_call_gene==j_call_gene[i], junction_length==junction_length[i], v_call_gene%in%str_split(DATA$v_call_gene[i],",")[[1]])
    if (nrow(temp)>1){
      DATA$v_call_gene[i] <- temp$v_call_gene[1]
      print(paste0("v-",i))
    }
  }
}


#### define clones:

DATA_p <- DATA %>% group_by(patient,v_call_gene,j_call_gene,junction_length) %>%
  summarise(n=n()) %>% arrange(patient, junction_length)

DATA_p <- DATA_p %>% group_by(patient) %>% 
  mutate(rn=row_number(),clone=paste0(patient,"_",rn,"_",n))


DATA_p_L <- left_join(DATA, DATA_p, by=c("patient", "v_call_gene", "j_call_gene", "junction_length"))

#### create germline! (d_mask)

write.table(DATA_p_L, file="/LC_data/DATA_p_L.tab",row.names = F, sep = "\t")

###########################################################
### creating refrence directory for create germline - V germline with gaps

write.fasta(as.list(c(VGERM_L,VGERM_K)),names = c(names(c(VGERM_L,VGERM_K))), file.out = '/germlines/LC/create_germline_ref_LC/V.fasta')
write.fasta(as.list(c(JGERM_L,JGERM_K)),names = c(names(c(JGERM_L,JGERM_K))), file.out = '/germlines/LC/create_germline_ref_LC/J.fasta')

system(paste0("source ", python_env, " && cd ","/LC_data; ",
              paste0('CreateGermlines.py ',
                     " -d /LC_data/DATA_p_L.tab",
                     " --outdir /LC_data",
                     " --cloned -g dmask --failed ",
                     " -r /germlines/LC/create_germline_ref_LC"))) #createGermline- the initial sequence that generated the same clone.

DATA_germ_p <- read.delim(file='/LC_data/DATA_p_L_germ-pass.tsv', sep='\t', stringsAsFactors = F) 

DATA_germ_L <- DATA_germ_p

############################################################################################

### load germlines: kV, kJ, hV, hJ

write.table(DATA_germ_L, file="/LC_data/DATA_germ_L_K.tab",row.names = F, sep = "\t")

source("internal_functions.R")

file <- "/LC_data/DATA_germ_L_K.tab"

# add mutations and baseline

f <- file
outpath <- "/LC_data"
### adds mutation profile and baseline columns to db
### INPUT: db and outpath
### OUTPUT: db with added columns

### read db
db <- read.delim(f, 
                 stringsAsFactors = F, header=T, sep = "\t")

cl <- makeCluster(4)

clusterExport(cl=cl,c("allele_diff","db"))

stopCluster(cl)
names(db)

## calc V and J mutations

if(grepl("K",f)){
  db <- claclMut(db, kV, kJ)
  model <- HKL_S5F
}else{
  db <- claclMut(db, hV, hJ)
  model <- HH_S5F
}

db$MUT <- db$mut_v+db$mut_j

# calc baseline
baseline <- calcBaseline(db, 
                         sequenceColumn="sequence_alignment",
                         germlineColumn="germline_alignment_d_mask", 
                         testStatistic="local",
                         regionDefinition=IMGT_V,
                         targetingModel=model,
                         nproc=4, calcStats = T)

db <- baseline@db
tmp <- baseline@stats
CDR <- tmp[tmp$region=="cdr",]
names_ <- names(CDR)[3:6]
names(CDR)[3:6] <- paste0("cdr_",tolower(names_))
db <- left_join(db,CDR[,c(1,3:6)],by="sequence_id")
FWR <- tmp[tmp$region=="fwr",]
names_ <- names(FWR)[3:6]
names(FWR)[3:6] <- paste0("fwr_",tolower(names_))
db <- left_join(db,FWR[,c(1,3:6)],by="sequence_id")

file <- gsub(".tab","",basename(f))
write.table(db, file = paste0(outpath, "/", file, "_baseline_mut.tab"),row.names = F, sep = "\t")
print(paste0(file, "_baseline_mut.tab"))

db_L <- db

db_L$barcode_L <- sub(".*aa_", "", db_L$sequence_id)
db_L$barcode_L <- paste0(db_L$patient,"_", db_L$barcode_L)

#### define clones

db_L$clone <- paste0(db_L$patient,"_",db_L$rn,"_",db_L$n)

######################################
#### find sub clones for each patient:

DATA7 <- db_L %>% filter(n>1)
unique_clone <- unique(DATA7$clone) 
DATA7$new_clone <- NA

##################################### heatmap between distance of cdr3 and V-genes ##################################

library(stringdist); library(RColorBrewer);library(gridExtra);
library(gplots);

ii <- 1
all_data <- NULL

for (ii in 1:length(unique_clone)){   #1:123
  
  s <- which(DATA7$clone==unique_clone[ii])
  
  d1 <- matrix(0,length(s),length(s))
  for (i in 1:length(s)){
    for (j in 1:length(s)){
      a <- as.character(DATA7$junction[s[i]])
      b <- as.character(DATA7$junction[s[j]])
      for (k in 1:nchar(as.character(DATA7$junction[s[1]]))){
        if (substr(a,k,k)!="N" & substr(b,k,k)!="N" & substr(a,k,k)!=substr(b,k,k)){
          d1[i,j]=d1[i,j]+1
        }
      }
    }
  }
  
  
  # d1=stringdistmatrix(DATA7$JUNCTION[s],DATA7$JUNCTION[s]) # I built the upper loop above to avoid counting N's as mutations.
  rownames(d1)=paste0(DATA7$clone[s],":",DATA7$sequence_id[s])
  colnames(d1)=paste0(DATA7$clone[s],":",DATA7$sequence_id[s])
  
  v_germ=substr(DATA7$sequence_alignment,start=DATA7$v_germline_start, stop=DATA7$v_germline_end)
  DATA7$V_GERM=v_germ
  
  for (ll in 1:length(s)){
    mmax=max(nchar(DATA7$V_GERM[s]))
    DATA7$V_GERM[s[ll]]=paste0(DATA7$V_GERM[s[ll]],strrep("N",mmax-nchar(DATA7$V_GERM[s[ll]])))
  }
  
  d2=matrix(0,length(s),length(s))
  for (i in 1:length(s)){
    for (j in 1:length(s)){
      a=as.character(DATA7$V_GERM[s[i]])
      b=as.character(DATA7$V_GERM[s[j]])
      if (nchar(a)!=nchar(b)){
        print("ERROR!!!")
      }
      for (k in 1:max(nchar(as.character(DATA7$V_GERM[s])))){
        if (substr(a,k,k)!="N" & substr(b,k,k)!="N" & substr(a,k,k)!="." & substr(b,k,k)!="." & substr(a,k,k)!=substr(b,k,k)){
          d2[i,j]=d2[i,j]+1
        }
      }
    }
  }
  
  # d2=stringdistmatrix(v_germ[s],v_germ[s])
  rownames(d2)=paste0(DATA7$clone[s],":",DATA7$sequence_id[s])
  colnames(d2)=paste0(DATA7$clone[s],":",DATA7$sequence_id[s])
  
  breaks=c(0,1,2,4,6,8,10,12,14) 
  breaks2=c(0,2,4,6,8,10,15,20,30,40)
  
  
  pdf(paste0("/LC_data/heatmap_junction_vgene-",unique_clone[ii],".pdf"),width=10,height=10)
  par(mfrow=c(2,1))
  
  gplots::heatmap.2(d1, cellnote = d1, notecol="black",
                    # main = "", # heat map title
                    density.info="none",  # turns off density plot inside color legend
                    trace="none",         # turns off trace lines inside the heat map
                    margins =c(20,20),     # widens margins around plot
                    col=brewer.pal(length(breaks)-1,"Blues"),
                    Rowv=T,Colv=T,
                    breaks=breaks,cexRow=1,cexCol=1,key.title="",main = paste0("Junction heatmap - clone ", unique_clone[ii]))
  
  gplots::heatmap.2(d2, cellnote = d2, notecol="black",
                    # main = "", # heat map title
                    density.info="none",  # turns off density plot inside color legend
                    trace="none",         # turns off trace lines inside the heat map
                    margins =c(20,20),     # widens margins around plot
                    col=brewer.pal(length(breaks2)-1,"Blues"),
                    Rowv=T,Colv=T,
                    breaks=breaks2,cexRow=1,cexCol=1,key.title="",main = paste0("V gene heatmap - clone ", unique_clone[ii]))
  
  dev.off()
  
  list1 <- list()
  ind=1
  for (i in 1:nrow(d2)){
    list1[[i]] <- names(which(d2[i,]<=15))
    ind <- ind+1
  }
  
  # find unique vectors   
  list2 <- list1[!duplicated(lapply(list1, sort))]  
  
  n <- length(list2) 
  
  len <- 0
  for (i in 1:n){
    len <- len+length(list2[[i]])
  }
  
  df <- data.frame(clone=rep(NA, len), sub_clone=rep(NA, len))
  
  ind=1
  index=1
  for (i in 1:n){
    len2 <- length(list2[[i]])
    df[ind:(ind+len2-1),1] <- list2[[i]]
    df[ind:(ind+len2-1),2] <- index
    index <- index+1
    ind <- ind+len2
  }
  
  df$sub_clone <- paste0(unique_clone[ii],"-",df$sub_clone)
  df$sequence_id <- sub(".*:", "", df$clone)
  df
  
  all_data <- rbind(all_data,df)
  ii <- ii+1
  ii
  
  
  ###### For clones that it waas not clear how to make the division to sub clones
  ###### because there was an overlap between groups with less than 15 mutations, 
  ###### I looked at the heatemp and made the division manually
  # len <- nrow(d2)
  # len
  # df <- data.frame(clone=rep(NA, len), sub_clone=rep(NA, len))
  # df$clone <- rownames(d2)
  # df$sequence_id <- sub(".*:", "", df$clone)
  # df
  # df$sub_clone <- c(1,1,2)
  # df <- df %>% arrange(sub_clone)
  # df
  # df$sub_clone <- paste0(unique_clone[ii],"-",df$sub_clone)
  # all_data <- rbind(all_data,df)
  # ii <- ii+1
  # ii
  
}

############
all_data_LC <- left_join(db_L, all_data%>%dplyr::select(-clone), by="sequence_id")

names(all_data_LC)[40] <- "clone_id"

save(/LC_data/all_data_LC.rda")
